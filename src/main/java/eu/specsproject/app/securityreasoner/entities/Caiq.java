package eu.specsproject.app.securityreasoner.entities;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType (propOrder={"CSP","documentId","associations"})
public class Caiq implements Serializable{ 

	private static final long serialVersionUID = -930854280328205368L;
	private String id;
	private String CSP;
	private String documentId;
	@XmlElementWrapper(name="associations")
	@XmlElement(name="association")
	private ArrayList<AssociatedTree> associations;//private HashMap<String, AssociatedTree> associations;

	public Caiq(){
		associations = new ArrayList<AssociatedTree>();
	}

	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCSP() {
		return CSP;
	}

	public void setCSP(String CSP) {
		this.CSP = CSP;
	}
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String id) {
		documentId = id;
	}

	public ArrayList<AssociatedTree> getAssociatedTrees() {
		return associations;
	}

	public void setAssociations(ArrayList<AssociatedTree> associations) {
		this.associations = associations;
	}

	public void addAssociatedTree(String idJudgement, String idTree, String state){
		AssociatedTree a = new AssociatedTree(idJudgement,idTree, state);
		associations.add(a);
	}

	public AssociatedTree getAssociatedTree(String idJudgement){
		for(AssociatedTree t : associations){
			if(t.getIdJudgement() != null && t.getIdJudgement().equals(idJudgement)){
				return t;
			}
		}
		return null;		
	}

	public boolean isAssociated(String idJudgement){
		return getAssociatedTree(idJudgement) != null;
	}

}
