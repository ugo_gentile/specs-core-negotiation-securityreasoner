package eu.specsproject.app.securityreasoner.utility;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class FromFileToString
  implements InterfaceFromFileToString
{
  public String convert(String xml_path)
    throws Exception
  {
    String xml = null;
    try
    {
      FileInputStream file = new FileInputStream(xml_path);
      InputStreamReader input = new InputStreamReader(file, "UTF-8");
      Reader reader = new BufferedReader(input);
      StringBuffer buffer = new StringBuffer();
      int ch;
      while ((ch = reader.read()) > -1)
      {
        buffer.append((char)ch);
      }
      reader.close();
      
      xml = buffer.toString();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new Exception();
    }
    return xml;
  }
}