package eu.specsproject.app.securityreasoner.evaluation;

import java.util.Collection;
import java.util.Iterator;
import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.elaboration.tree_rem_comparators.impl.TreeRemComparator;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeRem;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.DelegateFactoryTreeRem;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryWeightedEdge;
import eu.specs.negotiation.rem.representation.parsers.impl.TreeRemInstantiator;
import eu.specs.negotiation.rem.representation.parsers.impl.XMLRemInstantiator;
import eu.specs.negotiation.rem.utils.FromStringToFile;

// TODO: Auto-generated Javadoc
/**
 * The Class ExecTreeRemComparator.
 */
public class ExecTreeRemComparator {

	/**
	 * Exec.
	 *
	 * @param xml1 the xml1
	 * @param xml2 the xml2
	 * @param xmlout the xmlout
	 * @param outputTreeRemComparator the output tree rem comparator
	 * @param xsdTreeRem the xsd tree rem
	 */
	public void exec(String xml1, String xml2, String xmlout,String outputTreeRemComparator,String xsdTreeRem) {

		InterfaceFactoryTreeRem factory_tree = new DelegateFactoryTreeRem();
		InterfaceFactoryRequirementNode factory_node = new FactoryRequirementNode();
		InterfaceFactoryWeightedEdge factory_edge = new FactoryWeightedEdge();

		TreeRemInstantiator instantiator = new TreeRemInstantiator(
				factory_tree, factory_node, factory_edge);

		// String xmlTreeOut=null;

		try {

			String xmlPath1 = xml1;
			String xmlPath2 = xml2;

			// output

			String outPath = outputTreeRemComparator;
			String xsdPath = xsdTreeRem;

			Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree1 = instantiator
					.instantiate(xmlPath1,xsdPath);
			Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree2 = instantiator
					.instantiate(xmlPath2,xsdPath);

			TreeRemComparator difference_evaluator = new TreeRemComparator(
					factory_tree, factory_node, factory_edge);

			Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree_final = difference_evaluator
					.compare(tree1, tree2);

			XMLRemInstantiator instantiator_out = new XMLRemInstantiator();
			String xml_out = instantiator_out.instantiate(tree_final);

			FromStringToFile converter_out = new FromStringToFile();
			// converter_out.convert(xml_out,"./XMLFiles/comparedTree/ComparedTree"+xmlout+".xml");
			converter_out.convert(xml_out, outPath + xmlout + ".xml");

			Collection<InterfaceRequirementNode> collection_out = tree_final
					.getVertices();
			Iterator<InterfaceRequirementNode> iterator_out = collection_out
					.iterator();
			InterfaceRequirementNode node_out;
			String name_out;
			Double s_out;

			System.out.println("\n\n=========================");

			for (int i = 0; i < collection_out.size(); i++) {

				node_out = iterator_out.next();
				s_out = node_out.getSl();
				name_out = node_out.getName();
				System.out.println(name_out + " " + s_out);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// return xmlTreeOut;

	}

}