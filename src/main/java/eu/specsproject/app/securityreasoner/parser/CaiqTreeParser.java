package eu.specsproject.app.securityreasoner.parser;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.DelegateFactoryTreeRem;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryWeightedEdge;
import eu.specs.negotiation.rem.representation.parsers.impl.TreeRemInstantiator;
import eu.specs.negotiation.rem.representation.parsers.impl.XMLRemInstantiator;
import eu.specsproject.app.securityreasoner.utility.FromFileToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;

public class CaiqTreeParser {
	
	public static Tree<InterfaceRequirementNode, InterfaceWeightedEdge> unmarshall(String xml){
		FromFileToString converter = new FromFileToString();
		String path = (CaiqTreeParser.class.getResource("/")).toString().substring(5).replace("%20", " ");
		String xsd;
		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree = null;
		try {
			// Unmarshalling
			xsd = converter.convert(path + PropertiesManager.getProperty("xsdTreeRem"));
			eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeRem factory_tree = new DelegateFactoryTreeRem();
			eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryRequirementNode factory_node = new FactoryRequirementNode();
			eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryWeightedEdge factory_edge = new FactoryWeightedEdge();
			TreeRemInstantiator tri = new TreeRemInstantiator(factory_tree, factory_node, factory_edge);
			tree = tri.instantiate(xml, xsd);
		}catch(Exception e){
			e.printStackTrace(); 
		}
		return tree;
	}

	public static String marshall(Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree){
		XMLRemInstantiator instantiator_out = new XMLRemInstantiator();		
		String xml = "";
		try {
			xml = instantiator_out.instantiate(tree);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}

}
