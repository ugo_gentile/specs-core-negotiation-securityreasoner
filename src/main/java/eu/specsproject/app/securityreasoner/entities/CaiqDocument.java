package eu.specsproject.app.securityreasoner.entities;

import java.io.Serializable;

public class CaiqDocument implements Serializable{
	
	private static final long serialVersionUID = -1425176158087978066L;
	private String id;
    private String caiqXmlDocument;

    public CaiqDocument(){
       //Zero arguments constructor
    }
    
    public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public CaiqDocument(String doc){
        this.caiqXmlDocument = doc;
    }

    public String getcaiqXmlDocument() {
        return caiqXmlDocument;
    }

    public void setcaiqXmlDocument(
            String caiqXmlDocument) {
        this.caiqXmlDocument = caiqXmlDocument;
    }

}
