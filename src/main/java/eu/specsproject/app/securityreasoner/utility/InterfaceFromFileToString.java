package eu.specsproject.app.securityreasoner.utility;

public abstract interface InterfaceFromFileToString
{
  public abstract String convert(String paramString)
    throws Exception;
}
