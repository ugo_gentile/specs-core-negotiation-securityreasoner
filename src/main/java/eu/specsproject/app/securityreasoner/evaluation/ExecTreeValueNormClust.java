package eu.specsproject.app.securityreasoner.evaluation;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.elaboration.normalizator_clusterizator.general.impl.TreeValueNormClust;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRelationNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryRelationNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeRelation;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeRem;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.DelegateFactoryTreeRelation;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.DelegateFactoryTreeRem;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryRelationNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryWeightedEdge;
import eu.specs.negotiation.rem.representation.parsers.impl.TreeRelationInstantiator;
import eu.specsproject.app.securityreasoner.parser.CaiqParser;


// TODO: Auto-generated Javadoc
/**
 * The Class that invokes the ExecTreeValueNormClust.'s method of specs-evaluation framework for the evaluation.
 */
public class ExecTreeValueNormClust {
	
	/**
	 *  The method makes the Tree Normalization-Clustering.
	 *
	 * @param path the path
	 * @param xsdValueTreePath the xsd value tree path
	 * @param xmlRelationTreePath the xml relation tree path
	 * @param xsdRelationTreePath the xsd relation tree path
	 * @return the normalized-clustered tree
	 */
	public Tree<InterfaceRequirementNode, InterfaceWeightedEdge> exec(String caiqXML,String xsdValueTreePath, String xmlRelationTreePath,String xsdRelationTreePath) {

		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree_outPut = null;

		Tree<InterfaceValueNode, InterfaceEdge> tree_in_1 = CaiqParser.unmarshall(caiqXML);

		// Tree Relation
		Tree<InterfaceRelationNode, InterfaceEdge> tree_in_2 = null;
		InterfaceFactoryTreeRelation factory_tree2 = new DelegateFactoryTreeRelation();
		InterfaceFactoryRelationNode factory_node2 = new FactoryRelationNode();
		InterfaceFactoryEdge factory_edge2 = new FactoryEdge();

		TreeRelationInstantiator tree_2 = new TreeRelationInstantiator(
				factory_tree2, factory_node2, factory_edge2);
		try {
			String xmlPath = xmlRelationTreePath;
			String xsdPath = xsdRelationTreePath;
			// @SuppressWarnings("unused")
			tree_in_2 = tree_2.instantiate(xmlPath,xsdPath);

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Tree Norm-Clust
		InterfaceFactoryTreeRem factory_tree = new DelegateFactoryTreeRem();
		InterfaceFactoryRequirementNode factory_node = new FactoryRequirementNode();
		InterfaceFactoryWeightedEdge factory_edge = new FactoryWeightedEdge();

		TreeValueNormClust instantiator = new TreeValueNormClust(factory_tree,
				factory_node, factory_edge);

		double map[] = new double[2];
		map[0] = 1.0;
		map[1] = 3.0;

		try {

			Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree_in = instantiator
					.clusterize(tree_in_1, tree_in_2, map);
			tree_outPut = tree_in;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tree_outPut;
	}
}