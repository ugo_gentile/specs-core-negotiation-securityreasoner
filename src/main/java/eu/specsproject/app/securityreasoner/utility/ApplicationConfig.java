package eu.specsproject.app.securityreasoner.utility;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class ApplicationConfig extends ResourceConfig {
	
	public ApplicationConfig() {
        packages("eu.specsproject.app.securityreasoner.frontend");
        register(MultiPartFeature.class);
    }

}
