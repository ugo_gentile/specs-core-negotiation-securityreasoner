package eu.specsproject.app.securityreasoner.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "score")
public class CategoryScore implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8477056850562670983L;
	private String category;
	private Double score;
	
	public CategoryScore(){		
	}
	
	public CategoryScore(String category, Double score){
		this.category = category;
		this.score = score;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	
	

}
