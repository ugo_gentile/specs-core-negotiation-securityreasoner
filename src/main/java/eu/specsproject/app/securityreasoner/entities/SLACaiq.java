package eu.specsproject.app.securityreasoner.entities;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class SLACaiq {

	private String id;
	private String csp;
	private String slaCaiqXmlDocument;
	private double rootScore;
	@XmlElementWrapper(name="scores")
	@XmlElement(name="score")
	private ArrayList<CategoryScore> scores;

	public SLACaiq(){
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCsp() {
		return csp;
	}

	public void setCsp(String csp) {
		this.csp = csp;
	}

	public String getSLACaiqXmlDocument() {
		return slaCaiqXmlDocument;
	}

	public void setSLACaiqXmlDocument(String xmlDocument) {
		slaCaiqXmlDocument = xmlDocument;
	}

	public double getRootScore() {
		return rootScore;
	}

	public void setRootScore(double score) {
		rootScore = score;
	}

	public ArrayList<CategoryScore> getScores() {
		return scores;
	}

	public void setScores(ArrayList<CategoryScore> scores) {
		this.scores = scores;
	}
	
	public double getCategoryScore(String category){
		for(CategoryScore c : scores){
			if(c.getCategory() != null && c.getCategory().equals(category)){
				return c.getScore();
			}
		}
		return 0.0;
	}
	
}
