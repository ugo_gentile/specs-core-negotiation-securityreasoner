package eu.specsproject.app.securityreasoner.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.LoggerFactory;

import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqDocument;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.entities.Judgement;
import eu.specsproject.app.securityreasoner.entities.SLACaiq;

public class PersistenceImplementation {

	private EntityManagerFactory entityManagerFactory;

	public PersistenceImplementation(){
		entityManagerFactory = PersistenceEntityManager.getPersistenceEntityManager();
	}

	//-------------------------------------------------------------------------------------//
	//-------------------------- Caiq Persistence Implementation --------------------------//
	//-------------------------------------------------------------------------------------//

	public List<String> retrieveCaiqs(int start, int stop){
		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Caiq> cq = cb.createQuery(Caiq.class);

		List<Caiq> list;

		if(stop != -1){
			list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			list = em.createQuery(cq).setFirstResult(start).getResultList();
		}

		List<String> listId  = new ArrayList<String>();
		for (Caiq caiq :  list){
			listId.add(new String(caiq.getId()));
		}

		return listId;

	}

	public String createCaiq(String CSP, String caiqDocument) {

		if (caiqDocument==null)
			throw new IllegalArgumentException("The Caiq cannot be null");

		Caiq internalCaiq = new Caiq();
		LoggerFactory.getLogger(PersistenceImplementation.class).debug("json: "+caiqDocument);

		try{
			internalCaiq.setCSP(CSP);
		}catch(Exception e){e.printStackTrace();}
		internalCaiq.setDocumentId(storeCaiqDocument(caiqDocument));

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(internalCaiq);
		t.commit();
		em.close();

		return new  String(internalCaiq.getId());

	}

	public Caiq retrieveCaiq(String id){
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("Caiq_Identifier cannot be null");
		Caiq caiq = em.find(Caiq.class,id);

		if (caiq==null)
			throw new IllegalArgumentException("Caiq_Identifier is not valid");

		return caiq;
	}

	public void updateCaiq(String id,
			Caiq caiq){

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.merge(caiq);
		t.commit();
		em.close();       
	}

	public String removeCaiq(String id){

		if (id==null)
			throw new IllegalArgumentException("String cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		Caiq caiq = em.find(Caiq.class,id);

		if (caiq==null)
			throw new IllegalArgumentException("String is not valid");

		em.getTransaction().begin();
		em.remove(caiq);
		em.getTransaction().commit();

		return id;
	}

	public CaiqDocument retrieveCaiqDocument(String id){

		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("Caiq_Identifier cannot be null");
		CaiqDocument caiq = em.find(CaiqDocument.class,id);

		if (caiq==null)
			throw new IllegalArgumentException("Caiq_Identifier is not valid");

		return caiq;

	}

	public String storeCaiqDocument(String caiqDocument){

		CaiqDocument caiq = new CaiqDocument();
		caiq.setcaiqXmlDocument(caiqDocument);

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(caiq);
		t.commit();
		em.close();

		return caiq.getId();

	}

	public String removeCaiqDocument(String id) {

		if (id==null)
			throw new IllegalArgumentException("CaiqDocumentId cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		CaiqDocument caiqDocument = em.find(CaiqDocument.class,id);

		if (caiqDocument==null)
			throw new IllegalArgumentException("caiqDocument is not valid");

		em.getTransaction().begin();
		em.remove(caiqDocument);
		em.getTransaction().commit();

		return id;		
	}


	//-------------------------------------------------------------------------------------//
	//----------------------- Judgement Persistence Implementation ------------------------//
	//-------------------------------------------------------------------------------------//


	public List<Judgement> retrieveJudgements(int start, int stop){
		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Judgement> cq = cb.createQuery(Judgement.class);

		List<Judgement> list;

		if(stop != -1){
			list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			list = em.createQuery(cq).setFirstResult(start).getResultList();
		}

		return list;        
	}

	public String createJudgement(Judgement judgement) {

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(judgement);
		t.commit();
		em.close();        

		return new  String(judgement.getId());        
	}

	public Judgement retrieveJudgement(String id){
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("Judgement_Identifier cannot be null");
		Judgement judgement = em.find(Judgement.class,id);

		if (judgement==null)
			throw new IllegalArgumentException("Judgement_Identifier is not valid");

		return judgement;
	}

	public Judgement retrieveDefaultJudgement(){

		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Judgement> cq = cb.createQuery(Judgement.class);

		Root<Judgement> root = cq.from(Judgement.class);
		cq.where(cb.equal(root.get("defaultJudgement"), true));
		return em.createQuery(cq).getSingleResult();

		//return judgement;
	}

	public void updateJudgement(String id,
			Judgement judgement){

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.merge(judgement);
		t.commit();
		em.close();       
	}


	public String removeJudgement(String id){

		if (id==null)
			throw new IllegalArgumentException("String cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		Judgement judgement = em.find(Judgement.class,id);

		if (judgement==null)
			throw new IllegalArgumentException("String is not valid");

		em.getTransaction().begin();
		em.remove(judgement);
		em.getTransaction().commit();

		return id;
	}


	//-------------------------------------------------------------------------------------//
	//--------------------CaiqTree Persistence Implementation -----------------------------//
	//-------------------------------------------------------------------------------------//

	public CaiqTree retrieveCaiqTree(String id){
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("EvaluatedCaiq_Identifier cannot be null");
		CaiqTree caiqTree = em.find(CaiqTree.class,id);

		if (caiqTree==null)
			throw new IllegalArgumentException("EvaluatedCaiq_Identifier is not valid");

		return caiqTree;
	}

	public String createCaiqTree(CaiqTree caiqTree) {

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(caiqTree);
		t.commit();
		em.close();        

		return caiqTree.getId();        
	}

	public void updateCaiqTree(String id,
			CaiqTree caiqTree){

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.merge(caiqTree);
		t.commit();
		em.close();       
	}

	public String removeCaiqTree(String id){

		if (id==null)
			throw new IllegalArgumentException("String cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		CaiqTree caiqTree = em.find(CaiqTree.class,id);

		if (caiqTree==null)
			throw new IllegalArgumentException("String is not valid");

		em.getTransaction().begin();
		em.remove(caiqTree);
		em.getTransaction().commit();

		return id;
	}

	//-------------------------------------------------------------------------------------//
	//--------------------SLACaiq Persistence Implementation -----------------------------//
	//-------------------------------------------------------------------------------------//

	public String createSLACaiq(SLACaiq slaCaiq) {

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(slaCaiq);
		t.commit();
		em.close();        

		return slaCaiq.getId();      
	}

	public List<SLACaiq> retrieveSLACaiqs(int start, int stop) {
		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SLACaiq> cq = cb.createQuery(SLACaiq.class);

		List<SLACaiq> list;

		if(stop != -1){
			list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			list = em.createQuery(cq).setFirstResult(start).getResultList();
		}

		/*List<String> listId  = new ArrayList<String>();
		for (SLACaiq slaCaiq :  list){
			listId.add(new String(slaCaiq.getId()));
		}*/

		return list;

	}

	public SLACaiq retrieveSLACaiq(String id) throws IllegalArgumentException{
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("SlaCaiq_Identifier cannot be null");
		SLACaiq slaCaiq = em.find(SLACaiq.class,id);

		if (slaCaiq==null)
			throw new IllegalArgumentException("SlaCaiq_Identifier is not valid");

		return slaCaiq;
	}


}
