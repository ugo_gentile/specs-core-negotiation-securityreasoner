package eu.specsproject.app.securityreasoner.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceEntityManager {
	
	private static EntityManagerFactory entityManagerFactory;
	
	public static EntityManagerFactory getPersistenceEntityManager(){
		return entityManagerFactory == null ? Persistence.createEntityManagerFactory("unit_test") : entityManagerFactory;
	}
}
