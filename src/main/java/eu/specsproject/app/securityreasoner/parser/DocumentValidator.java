package eu.specsproject.app.securityreasoner.parser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class DocumentValidator {
	
	public static boolean validate(InputStream xml, InputStream xsd)
	{
		try
		{
			SchemaFactory factory = 
					SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			Schema schema = factory.newSchema(new StreamSource(xsd));
			Validator validator = schema.newValidator();

			Reader reader = new InputStreamReader(xml,"UTF-8");

			validator.validate(new StreamSource(reader));
			return true;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
	}

}
