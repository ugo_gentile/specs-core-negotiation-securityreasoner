package eu.specsproject.app.securityreasoner.frontend;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import eu.specsproject.app.securityreasoner.entities.AssociatedTree;
import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.Judgement;
import eu.specsproject.app.securityreasoner.parser.DocumentValidator;
import eu.specsproject.app.securityreasoner.parser.JudgementResourceParser;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;
import eu.specsproject.app.securityreasoner.utility.FromInputStreamToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;

public class JudgementResource {

	private String id;

	public JudgementResource (String id){
		this.id=new String(id);
	}

	@GET
	@Produces({MediaType.APPLICATION_XML})
	public Response getJudgement(){
		PersistenceImplementation pi = new PersistenceImplementation();
		try {
			return Response.status(200).type("text/xml")
					.entity(JudgementResourceParser.marshal(pi.retrieveJudgement(id)))
					.type(MediaType.APPLICATION_XML).build();
		} catch (JAXBException e) {
			return Response.status(404).type("text/plain")
					.entity("Not Found: Caiq not found.").build();
		}
	}

	@PUT
	@Consumes(MediaType.TEXT_PLAIN)
	public Response updateJudgement(String newJudgement){

		PersistenceImplementation pi = new PersistenceImplementation();
		for(String caiqId : pi.retrieveCaiqs(0, -1)){
			Caiq caiq = pi.retrieveCaiq(caiqId);
			if(caiq.isAssociated(id)){
				return Response.status(409).type("text/plain")
						.entity("Conflict: there's already at least a caiq associated to this judgement.").build();
			}
		}

		Judgement judgement = pi.retrieveJudgement(id);

		if (judgement!=null){

			InputStream xsd = null;
			try {
				xsd = new FileInputStream((this.getClass().getResource("/")).toString().substring(5).replace("%20", " ") + PropertiesManager.getProperty("xsdWeightsSimple"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			// Awful solution, but didn't work differently 
			if(DocumentValidator.validate(new ByteArrayInputStream(newJudgement.getBytes()), new ByteArrayInputStream(FromInputStreamToString.convert(xsd).getBytes()))){

				judgement.setJudgementXmlDocument(newJudgement);
				pi.updateJudgement(id, judgement);

				return Response.status(200).type("text/plain")
						.entity(judgement.getId()).build();
			}
			else{
				return Response.status(435).type("text/plain")
						.entity("Invalid Input: request body not compliant with the defined schema.").build();
			}
		}
		else{
			return Response.status(404).type("text/plain")
					.entity("Not Found: the specified caiq has not been found.").build();
		}
	}

	//TODO IMPORTANT: on deletion of a judgement must be removed every caiq association referencing it, and every caiqtree related 
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public Response removeJudgement(){
		PersistenceImplementation pi = new PersistenceImplementation();
		if(pi.retrieveJudgement(id).isDefaultJudgement())
			return Response.status(409).type("text/plain")
					.entity("Conflict: cannot delete the default judgement.").build();
		for(String caiqId : pi.retrieveCaiqs(0, -1)){
			Caiq caiq = pi.retrieveCaiq(caiqId);
			if(caiq.isAssociated(id)){
				pi.removeCaiqTree(caiq.getAssociatedTree(id).getIdTree());

				ArrayList<AssociatedTree> list = caiq.getAssociatedTrees();
				Iterator<AssociatedTree> it = list.iterator();
				while(it.hasNext()){
					if(it.next().getIdJudgement().equals(id))
						it.remove();
				}
				caiq.setAssociations(list);
				pi.updateCaiq(caiqId, caiq);
			}
		}
		try{
			pi.removeJudgement(id);
		}catch(IllegalArgumentException e){
			return Response.status(404).type("text/plain")
					.entity("Not Found: the specified judgement has not been found.").build();
		}
		return Response.status(204).type("text/plain")
				.entity("No Content: the judgement has been deleted.").build();//Response.ok(id.toString()).build();
	}
}
