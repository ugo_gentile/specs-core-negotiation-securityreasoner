package eu.specsproject.app.securityreasoner.frontend;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;

public class CaiqTreeResource {
	
	@Context
	UriInfo uriInfo;

	private String caiqId;

	public CaiqTreeResource (String id){
		this.caiqId=id;
	}

	@GET
	@Produces({MediaType.APPLICATION_XML })
	public Response evaluate(@Context UriInfo ui){

		MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
		String idJudgement = queryParams.getFirst("idJudgement");
		
		// If no argument is given to the call, evaluate the caiq with the default judgement
		if(idJudgement == null){
			try {
				idJudgement = new PersistenceImplementation().retrieveDefaultJudgement().getId();
			} catch (Exception e) {e.printStackTrace();}
		}
		
		PersistenceImplementation pi = new PersistenceImplementation();
		Caiq caiq = pi.retrieveCaiq(caiqId);
		if(!caiq.isAssociated(idJudgement))
			return Response.status(409).type("text/plain")
	                .entity("Conflict: caiq not associated to this judgement.").build();
		else{		
			CaiqTree caiqTree = pi.retrieveCaiqTree(caiq.getAssociatedTree(idJudgement).getIdTree());
			// If state equals "weighted", evaluate the caiq...
			if(caiq.getAssociatedTree(idJudgement).getState().equals("weighted")){
				caiqTree.evaluate();
				
				// Update stored info
				pi.updateCaiqTree(caiqTree.getId(), caiqTree);
				caiq.getAssociatedTree(idJudgement).setState("evaluated");
				pi.updateCaiq(caiq.getId(), caiq);
			}
			// ...Otherwise do nothing, anyway return its id
			return Response.status(201).type("text/xml")
            .entity(caiqTree.getCaiqTreeXMLDocument()).build();
			/*return Response.status(201).type("text/plain")
	                .entity(caiqTree.getId()).build();*/
		}
	}


}
