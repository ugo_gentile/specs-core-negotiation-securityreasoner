package eu.specsproject.app.securityreasoner.evaluation;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.elaboration.tree_weights_evaluators.InterfaceTreeSimpleWeightsEvaluator;
import eu.specs.negotiation.rem.elaboration.tree_weights_evaluators.impl.TreeSimpleWeightsEvaluator;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceSimpleJudgmentNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactorySimpleJudgmentNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeSimpleWeights;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.DelegateFactoryTreeSimpleWeights;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactorySimpleJudgmentNode;
import eu.specs.negotiation.rem.representation.parsers.impl.TreeSimpleWeightsInstantiator;

// TODO: Auto-generated Javadoc
/**
 * The Class that invokes the TreeSimpleWeightsInstantiator's method of specs-evaluation framework for the evaluation.
 */
public class ExecTreeSimpleWeightsInstantiator {
	
	/**
	 * This method makes the Tree Weighting.
	 *
	 * @param treeClustered the tree clustered
	 * @param xmlinputJudgmentPath the xmlinput judgment path
	 * @param xsdWeightsSimplePath the xsd weights simple path
	 * @return the weighted tree 
	 */
	public Tree<InterfaceRequirementNode, InterfaceWeightedEdge> exec(Tree<InterfaceRequirementNode, InterfaceWeightedEdge> treeClustered,String xmlinputJudgmentPath,String xsdWeightsSimplePath) {

		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> treeOut = null;

		try {
			//Judgment
			String xmlJudgmentPath = xmlinputJudgmentPath;
			String xsdWeightPath = xsdWeightsSimplePath;
						
			InterfaceFactoryTreeSimpleWeights factory_tree_weights = new DelegateFactoryTreeSimpleWeights();
			InterfaceFactorySimpleJudgmentNode factory_node_weights = new FactorySimpleJudgmentNode();
			InterfaceFactoryEdge factory_edge_weights = new FactoryEdge();
			TreeSimpleWeightsInstantiator instantiator_in_weights = new TreeSimpleWeightsInstantiator(
					factory_tree_weights, factory_node_weights,
					factory_edge_weights);
			Tree<InterfaceSimpleJudgmentNode, InterfaceEdge> tree_in_weights = instantiator_in_weights
					.instantiate(xmlJudgmentPath,xsdWeightPath);

			InterfaceTreeSimpleWeightsEvaluator evaluator = new TreeSimpleWeightsEvaluator();
			treeOut = evaluator
					.weighting(treeClustered, tree_in_weights);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return treeOut;
	}

}