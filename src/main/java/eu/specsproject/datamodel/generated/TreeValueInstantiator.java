package eu.specsproject.datamodel.generated;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeValue;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryValueNode;
import eu.specs.negotiation.rem.representation.parsers.InterfaceTreeValueInstantiator;
import eu.specs.negotiation.rem.representation.parsers.exceptions.NodeAlreadyPresentException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Vector;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class TreeValueInstantiator
  implements InterfaceTreeValueInstantiator
{
  private InterfaceFactoryTreeValue factory_tree;
  private InterfaceFactoryValueNode factory_node;
  private InterfaceFactoryEdge factory_edge;
  private Vector<String> vett;
  
  public TreeValueInstantiator(InterfaceFactoryTreeValue factory_tree, InterfaceFactoryValueNode factory_node, InterfaceFactoryEdge factory_edge)
  {
    this.factory_tree = factory_tree;
    this.factory_node = factory_node;
    this.factory_edge = factory_edge;
  }
  
  public Tree<InterfaceValueNode, InterfaceEdge> instantiate(String xmlString, String xsdString)
    throws NodeAlreadyPresentException
  {
    Tree<InterfaceValueNode, InterfaceEdge> jung_tree = this.factory_tree
      .createTreeValue();
    
    InputStream xl = new ByteArrayInputStream(xmlString.getBytes());
    InputStream xsd = new ByteArrayInputStream(xsdString.getBytes());
    validate(xl, xsd);
    
    String xml = xmlString;
    
    this.vett = new Vector();
    try
    {
      XMLInputFactory inputFactory = XMLInputFactory.newInstance();
      inputFactory.setProperty("javax.xml.stream.isCoalescing", 
        Boolean.TRUE);
      inputFactory.setProperty(
        "javax.xml.stream.isSupportingExternalEntities", 
        Boolean.FALSE);
      XMLStreamReader streamReader = inputFactory
        .createXMLStreamReader(new StringReader(xml));
      
      String textIdValue = null;
      boolean cont = false;
      
      InterfaceValueNode root = null;
      while (streamReader.hasNext())
      {
        int eventType = streamReader.next();
        if (eventType == 1)
        {
          QName name = streamReader.getName();
          String sname = name.toString();
          if (sname.equalsIgnoreCase("ROOT"))
          {
            streamReader.getAttributeCount();
            name = streamReader.getAttributeName(0);
            name.toString();
            
            textIdValue = streamReader.getAttributeValue(0);
            name.toString();
            
            root = this.factory_node.createValueNode(textIdValue);
            
            jung_tree.addVertex(root);
            do
            {
              cont = firstLevelParsing(root, jung_tree, 
                streamReader);
            } while (!cont);
          }
        }
        else
        {
          if ((eventType == 8) || 
            (cont)) {
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return jung_tree;
  }
  
  private boolean firstLevelParsing(InterfaceValueNode root, Tree<InterfaceValueNode, InterfaceEdge> jung_tree, XMLStreamReader streamReader)
    throws Exception
  {
    String textId = null;
    boolean exit = false;
    while (!exit)
    {
      int eventType = streamReader.nextTag();
      if (eventType == 2)
      {
        exit = true;
      }
      else
      {
        streamReader.getName();
        
        textId = streamReader.getAttributeValue(0);
        if (this.vett.contains(textId)) {
          throw new NodeAlreadyPresentException(textId);
        }
        this.vett.add(textId);
        
        InterfaceValueNode child = this.factory_node.createValueNode(textId);
        
        InterfaceEdge edge = this.factory_edge.createEdge();
        
        jung_tree.addEdge(edge, root, child);
        
        secondLevelParsing(streamReader, child, jung_tree);
      }
    }
    return exit;
  }
  
  private boolean secondLevelParsing(XMLStreamReader streamReader, InterfaceValueNode child, Tree<InterfaceValueNode, InterfaceEdge> jung_tree)
    throws NodeAlreadyPresentException
  {
    String textId = null;
    boolean exit = false;
    
    InterfaceValueNode node = null;
    InterfaceValueNode fatherNode = child;
    try
    {
      while (!exit)
      {
        int eventType = streamReader.nextTag();
        if (eventType == 2)
        {
          exit = true;
        }
        else
        {
          streamReader.getName();
          
          textId = streamReader.getAttributeValue(0);
          if (this.vett.contains(textId)) {
            throw new NodeAlreadyPresentException(textId);
          }
          this.vett.add(textId);
          
          node = this.factory_node.createValueNode(textId);
          
          InterfaceEdge edge = this.factory_edge.createEdge();
          
          jung_tree.addEdge(edge, fatherNode, node);
          
          parseValue(streamReader, node, jung_tree);
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return exit;
  }
  
  private boolean parseValue(XMLStreamReader streamReader, InterfaceValueNode child, Tree<InterfaceValueNode, InterfaceEdge> jung_tree)
    throws NodeAlreadyPresentException
  {
    String textId = null;
    boolean exit = false;
    
    InterfaceValueNode node = null;
    InterfaceValueNode fatherNode = child;
    try
    {
      while (!exit)
      {
        int eventType = streamReader.nextTag();
        if (eventType == 2)
        {
          exit = true;
        }
        else
        {
          streamReader.getName();
          
          textId = streamReader.getAttributeValue(0);
          if (this.vett.contains(textId)) {
            throw new NodeAlreadyPresentException(textId);
          }
          this.vett.add(textId);
          
          node = this.factory_node.createValueNode(textId);
          
          InterfaceEdge edge = this.factory_edge.createEdge();
          
          jung_tree.addEdge(edge, fatherNode, node);
          
          eventType = streamReader.nextTag();
          
          eventType = streamReader.next();
          
          String val = streamReader.getText();
          
          node.setValue(val);
          
          eventType = streamReader.next();
          
          eventType = streamReader.nextTag();
        }
      }
    }
    catch (XMLStreamException e)
    {
      e.printStackTrace();
    }
    return exit;
  }
  
  private void validate(InputStream xml, InputStream xsd)
  {
    try
    {
      SchemaFactory factory = 
        SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
      Schema schema = factory.newSchema(new StreamSource(xsd));
      Validator validator = schema.newValidator();
      
      Reader reader = new InputStreamReader(xml,"UTF-8");
      
      validator.validate(new StreamSource(reader));
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
