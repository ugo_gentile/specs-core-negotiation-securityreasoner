package eu.specsproject.app.securityreasoner.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "association")
public class AssociatedTree implements Serializable{

	private static final long serialVersionUID = -8945717608318113667L;
	private String idJudgement;
	private String idTree;
	private String state;
	
	public AssociatedTree(){}
	
	public AssociatedTree(String judgementId, String treeId, String state){
		this.idJudgement = judgementId;
		this.idTree = treeId;
		this.state = state;
	}
	
	public String getIdJudgement() {
		return idJudgement;
	}

	public void setIdJudgement(String idJudgement) {
		this.idJudgement = idJudgement;
	}

	public String getIdTree() {
		return idTree;
	}
	public void setIdTree(String id) {
		this.idTree = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
			
}