package eu.specsproject.app.securityreasoner.utility;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {
    
    public static String getProperty(String key){
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "config.properties";
            input = PropertiesManager.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("Sorry, unable to find " + filename);
                return "";
            }

            //load a properties file from class path, inside static method
            prop.load(input);

            return prop.getProperty(key);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return "";
    }
    
    public static void setProperty(String key, String value){
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "config.properties";
            input = PropertiesManager.class.getClassLoader().getResourceAsStream(filename);
            
            if(input==null){
                System.out.println("Sorry, unable to find " + filename);
            }

            //load a properties file from class path, inside static method
            prop.load(input);
            prop.setProperty(key,value);
            
            prop.store(new FileOutputStream( PropertiesManager.class.getResource("/").toString().substring(5).replace("%20", " ") + filename), null);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}