package eu.specsproject.app.securityreasoner.utility;

public abstract interface InterfaceFromStringToFile
{
  public abstract void convert(String paramString1, String paramString2)
    throws Exception;
}
