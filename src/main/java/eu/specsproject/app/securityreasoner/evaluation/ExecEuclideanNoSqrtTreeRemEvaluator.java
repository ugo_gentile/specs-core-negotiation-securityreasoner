package eu.specsproject.app.securityreasoner.evaluation;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.elaboration.tree_rem_evaluators.impl.EuclideanNoSqrtTreeRemEvaluator;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.parsers.impl.XMLRemInstantiator;

// TODO: Auto-generated Javadoc
/**
 * The Class that invokes the EuclideanNoSqrtTreeRemEvaluator's method of specs-evaluation framework for the evaluation.
 */
public class ExecEuclideanNoSqrtTreeRemEvaluator {
	
	/**
	 * The method makes the Euclidean NoSqrt evaluation.
	 *
	 * @param clusteredTree the clustered tree
	 * @return the string of the evaluated tree
	 */
	public String exec(Tree<InterfaceRequirementNode, InterfaceWeightedEdge> clusteredTree) {

		String xml_out = null;

		try {
			
			// Evaluate EuclideanDistance.
			EuclideanNoSqrtTreeRemEvaluator evaluator = new EuclideanNoSqrtTreeRemEvaluator();
			Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree_out = evaluator
					.evaluate(clusteredTree);

			// Instantiate the output xml string.
			XMLRemInstantiator instantiator_out = new XMLRemInstantiator();
			xml_out = instantiator_out.instantiate(tree_out);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml_out;
	}
}