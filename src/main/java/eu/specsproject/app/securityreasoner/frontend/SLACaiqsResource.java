package eu.specsproject.app.securityreasoner.frontend;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.exceptions.SlNotSetException;
import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.entities.CategoryScore;
import eu.specsproject.app.securityreasoner.entities.Collection;
import eu.specsproject.app.securityreasoner.entities.Item;
import eu.specsproject.app.securityreasoner.entities.Judgement;
import eu.specsproject.app.securityreasoner.entities.SLACaiq;
import eu.specsproject.app.securityreasoner.parser.CaiqParser;
import eu.specsproject.app.securityreasoner.parser.CaiqTreeParser;
import eu.specsproject.app.securityreasoner.parser.DocumentValidator;
import eu.specsproject.app.securityreasoner.parser.SLAParser;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;
import eu.specsproject.app.securityreasoner.utility.FromInputStreamToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;

@Path("slacaiqs")
public class SLACaiqsResource {

	@Context
	UriInfo uriInfo;

	@GET
	@Produces({MediaType.APPLICATION_JSON })
	public Response getCaiqs(@QueryParam("items") Integer totalItems, @QueryParam("page") Integer page, @QueryParam("length") Integer length) {

		List<Item> items = new ArrayList<Item>();
		int start = (page != null && length != null && totalItems == null) ? page : 0; 
		int stop = (totalItems != null) ? totalItems : -1; 
		stop = (page != null && length != null && totalItems == null) ? page+length : stop; 

		PersistenceImplementation pi = new PersistenceImplementation();

		List<SLACaiq> slaCaiqs = pi.retrieveSLACaiqs(start, stop);

		java.util.Collections.sort(slaCaiqs, new Comparator<SLACaiq>(){
			public int compare(SLACaiq o1, SLACaiq o2){
				if(o1.getRootScore() == o2.getRootScore())
					return 0;
				return o1.getRootScore() < o2.getRootScore() ? -1 : 1;
			}
		});
		java.util.Collections.reverse(slaCaiqs);

		stop = slaCaiqs.size();
		for (int j = start; j < stop; j++){
			items.add(new Item(String.valueOf(slaCaiqs.get(j).getId()), uriInfo.getAbsolutePath()+"/"+slaCaiqs.get(j).getId()));
		}
		Collection collection = new Collection("SLA_CAIQ", slaCaiqs.size(), stop-start, items);
		GenericEntity<Collection> coll = new GenericEntity<Collection>(collection) {};
		return Response.ok(coll).build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.TEXT_PLAIN)
	public Response adjustCaiq(String sla) throws IOException{
		/*
		InputStream xsd = null;
		try {
			xsd = new FileInputStream((this.getClass().getResource("/")).toString().substring(5) + PropertiesManager.getProperty("xsdSlaCaiqTree"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(DocumentValidator.validate(new ByteArrayInputStream(sla.getBytes()), new ByteArrayInputStream(FromInputStreamToString.convert(xsd).getBytes()))){
		 */

		AgreementOffer offer = null;
		try{
			offer = SLAParser.buildOfferFromXml(sla);
		} catch (javax.xml.bind.JAXBException e) {
			return Response.status(435).type("text/plain")
					.entity("Invalid Input: request body not compliant with the defined schema.").build();
		}
		String csp = null;
		List<Term> terms = offer.getTerms().getAll().getAll();

		try{
			for (Term term : terms){
				if(term instanceof ServiceDescriptionTerm){
					ServiceDescriptionTerm serviceTerm = (ServiceDescriptionTerm) term;
					csp = serviceTerm.getServiceDescription().getServiceResources().get(0).getResourcesProvider().get(0).getName();
				}
			}
		}catch(NullPointerException e){

		}

		PersistenceImplementation pi = new PersistenceImplementation();		
		List<String> listId = pi.retrieveCaiqs(0,0);
		List<Caiq> listCaiq = new ArrayList<Caiq>();

		//Retrieve Caiq list
		for(int i=0;i<listId.size();i++){
			listCaiq.add(pi.retrieveCaiq(listId.get(i)));
		}

		//Search csp in caiq list
		String caiqXML = new String();
		boolean found = false;

		for (int i=0;i<listCaiq.size();i++){
			if(listCaiq.get(i).getCSP().toLowerCase().contains(csp.toLowerCase())){
				caiqXML = pi.retrieveCaiqDocument(listCaiq.get(i).getDocumentId()).getcaiqXmlDocument();
				found = true;
			}
		}

		if(!found){
			return Response.status(404).type("text/plain")
					.entity("Not Found: Caiq not available.").build();
		}
		else{

			//Modify csp's caiq		
			String modifiedCaiq = updateCaiqFromSla(sla,caiqXML);

			//Evaluation modifiedCaiq	
			CaiqTree tree = new CaiqTree();
			Judgement judgement = pi.retrieveDefaultJudgement();
			if(judgement==null){
				return Response.status(409).type("text/plain")
						.entity("Conflict: System not initialized.").build();
			}
			else{
				String judgementXML = judgement.getJudgementXmlDocument();				
				tree.buildWeightedTree(modifiedCaiq,judgementXML);
				tree.evaluate();		

				//Save SLACAiq
				SLACaiq slaCaiq = new SLACaiq();

				slaCaiq.setCsp(csp);
				slaCaiq.setSLACaiqXmlDocument(tree.getCaiqTreeXMLDocument());		

				Tree<InterfaceRequirementNode, InterfaceWeightedEdge> treeParsed = CaiqTreeParser.unmarshall(tree.getCaiqTreeXMLDocument());

				ArrayList<CategoryScore> scoreCategories = new ArrayList<CategoryScore>();
				java.util.Collection<InterfaceRequirementNode> collection = treeParsed.getChildren(treeParsed.getRoot());
				for (InterfaceRequirementNode node : collection){
					try {
						scoreCategories.add(new CategoryScore (node.getName(), node.getSl()));
					} catch (SlNotSetException e) {
						e.printStackTrace();
					}
				}

				slaCaiq.setScores(scoreCategories);

				try {
					slaCaiq.setRootScore(treeParsed.getRoot().getSl());
				} catch (SlNotSetException e) {
					e.printStackTrace();
				}

				String idSlaCaiq = pi.createSLACaiq(slaCaiq);

				return Response.status(201).type("text/plain")
						.entity(idSlaCaiq).build();
			}
		}

		/*}
		else{
			return Response.status(435).type("text/plain")
					.entity("Invalid Input: request body not compliant with the defined schema.").build();
		}
		 */ 
	}


	private String updateCaiqFromSla(String sla,String caiq){		

		List<String> ccmSecurityControlList = new ArrayList<String>();

		AgreementOffer offer = null;
		try{
			offer = SLAParser.buildOfferFromXml(sla);
		} catch (javax.xml.bind.JAXBException e) {
			return null;
		}

		List<Term> terms = offer.getTerms().getAll().getAll();
		List<AbstractSecurityControl> securityControl = new ArrayList<AbstractSecurityControl>();
		try{
			for (Term term : terms){
				if(term instanceof ServiceDescriptionTerm){

					ServiceDescriptionTerm serviceTerm = (ServiceDescriptionTerm) term;
					List<CapabilityType> capabilities = serviceTerm.getServiceDescription().getCapabilities().getCapability();

					for (CapabilityType capability : capabilities){

						securityControl = capability.getControlFramework().getSecurityControl();

						Pattern p = Pattern.compile("[A-Z]{3}-[0-9]{2}");
						Matcher matcher = p.matcher(securityControl.toString());
						while (matcher.find()) {				  
							ccmSecurityControlList.add(matcher.group());
						} 
					}			
				}
			}
		}catch(NullPointerException e){
		}

		Tree<InterfaceValueNode, InterfaceEdge> tree = CaiqParser.unmarshall(caiq);

		java.util.Collection<InterfaceValueNode> collection = tree.getVertices();
		for (InterfaceValueNode node : collection){
			if(ccmSecurityControlList.contains(node.getName())){
				for (InterfaceValueNode son : tree.getChildren(node) ){
					son.setValue("YES");
				}
			}
		}

		try {
			caiq = CaiqParser.marshall(tree);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caiq;
	}


	@Path("/{id}")
	public SLACaiqResource getSLACaiqScore(@PathParam("id") String id) {
		return new SLACaiqResource(id);
	}

}

