package eu.specsproject.app.securityreasoner.frontend;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.utils.FromFileToString;
import eu.specsproject.app.securityreasoner.entities.Collection;
import eu.specsproject.app.securityreasoner.entities.Item;
import eu.specsproject.app.securityreasoner.parser.CaiqParser;
import eu.specsproject.app.securityreasoner.parser.DocumentValidator;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;
import eu.specsproject.app.securityreasoner.utility.FromInputStreamToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;


@Path("caiqs")
public class CaiqsResource {

	@Context
	UriInfo uriInfo;

	@GET
	@Produces({MediaType.APPLICATION_JSON })
	public Response getCaiqs(@QueryParam("items") Integer totalItems, @QueryParam("page") Integer page, @QueryParam("length") Integer length) {

		List<Item> items = new ArrayList<Item>();
		int start = (page != null && length != null && totalItems == null) ? page : 0; 
		int stop = (totalItems != null) ? totalItems : -1; 
		stop = (page != null && length != null && totalItems == null) ? page+length : stop; 

		PersistenceImplementation pi = new PersistenceImplementation();

		List<String> caiqs = pi.retrieveCaiqs(start, stop);


		stop = caiqs.size();
		for (int j = start; j < stop; j++){
			items.add(new Item(caiqs.get(j), uriInfo.getAbsolutePath()+"/"+caiqs.get(j)));
		}
		Collection collection = new Collection("CAIQ", caiqs.size(), stop-start, items);
		GenericEntity<Collection> coll = new GenericEntity<Collection>(collection) {
			//Anonimous class
		};
		return Response.ok(coll).build();

	}

	/*@POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createCaiq(String document){
		Charset.forName("UTF-8").encode(document);
		String id = new PersistenceImplementation().createCaiq(document); 
        UriBuilder ub = uriInfo.getAbsolutePathBuilder();
        URI smURI = ub.
                path(id.toString()).
                build();
        return Response.created(smURI).entity(id.toString()).build();
    }*/

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createCaiq(
			@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader//,
			//@FormDataParam("name") String name
			){

		String CSP = contentDispositionHeader.getFileName();
		String document = FromInputStreamToString.convert(fileInputStream);
		InputStream xsd = null;
		try {
			xsd = new FileInputStream((this.getClass().getResource("/")).toString().substring(5).replace("%20", " ") + PropertiesManager.getProperty("xsdValueTree"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Awful solution, but didn't work differently 
		if(DocumentValidator.validate(new ByteArrayInputStream(document.getBytes()), new ByteArrayInputStream(FromInputStreamToString.convert(xsd).getBytes()))){

			//-----------------auto fill tree------------------------
			Tree<InterfaceValueNode, InterfaceEdge> treeIn = CaiqParser.unmarshall(document);
			
				//get leaves
			
			java.util.Collection<InterfaceValueNode> nodes = treeIn.getVertices();
			ArrayList<InterfaceValueNode> leaves = new ArrayList<InterfaceValueNode>();
			for (InterfaceValueNode node : nodes){
				if(treeIn.getChildCount(node) == 0){
					leaves.add(node);
				}
			}
				//retrieve caiq model tree
			FromFileToString converter = new FromFileToString();
			Tree<InterfaceValueNode, InterfaceEdge> treeModel = null;
			try {
				treeModel = CaiqParser.unmarshall(converter.convert((this.getClass().getResource("/")).toString().substring(5).replace("%20", " ") + PropertiesManager.getProperty("CaiqModel")));
			} catch (Exception e) {
				System.out.println("System Error: Cannot find caiq model");
				e.printStackTrace();
			}
			
				//scan Model to add positive answers
			for (InterfaceValueNode node : leaves){
				boolean found = false;
				for(InterfaceValueNode nodeModel : treeModel.getVertices()){
						if(node.getName().equals(nodeModel.getName())){
							nodeModel.setValue("YES");
							found = true;
							break;
						}
				}
				//If input control is not found, the input document is not compliant with the model
				if(!found)
					return Response.status(435).type("text/plain")
							.entity("Invalid Input: request body not compliant with the defined schema.").build();
			}

			String Newdocument = "";
			try {
				Newdocument = CaiqParser.marshall(treeModel);
			} catch (Exception e) {
				e.printStackTrace();
			}


			//-------------------------------------------------------
			
			String id = new PersistenceImplementation().createCaiq(CSP.split("\\.")[0], Newdocument);

			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI smURI = ub.
					path(id.toString()).
					build();
			return Response.created(smURI).entity(id.toString()).build();}
		else{
			return Response.status(435).type("text/plain")
					.entity("Invalid Input: request body not compliant with the defined schema.").build();
		}
	}	

	@Path("/{id}")
	public CaiqResource getCaiq(@PathParam("id") String id) {
		return new CaiqResource(id);
	}

}
