package eu.specsproject.app.securityreasoner.utility;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class FromStringToFile
  implements InterfaceFromStringToFile
{
  public void convert(String xml, String path)
    throws Exception
  {
    try
    {
      FileOutputStream file = new FileOutputStream(path);
      OutputStreamWriter writer = new OutputStreamWriter(file, "UTF-8");
      writer.write(xml);
      writer.close();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new Exception();
    }
  }
}