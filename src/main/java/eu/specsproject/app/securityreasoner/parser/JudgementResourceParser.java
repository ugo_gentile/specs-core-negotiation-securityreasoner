package eu.specsproject.app.securityreasoner.parser;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import eu.specsproject.app.securityreasoner.entities.Judgement;

public class JudgementResourceParser {

	public static String marshal(Judgement judgement) throws JAXBException{
		java.io.StringWriter sw = new StringWriter();

		JAXBContext jaxbContext = JAXBContext.newInstance(Judgement.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(judgement, sw);

		return sw.toString();

	}

}
